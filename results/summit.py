#!/bin/python

# TODO: Rewrite this script to calculate the average accuracy from .csv files
# This script is currently *not* up to date with the rest of the code

import re
import os

# to search for accuracy in .cm file:
pat = re.compile("(\d+\.\d+)%")

# average of .cm file:
def cmavg(files):
    avg = 0

    for fn in files:
        with open(fn) as f:
            lines = f.readlines()
            avg += float(pat.search(lines[-1]).group(1))

    avg /= len(files)
    return avg

files = [f for f in os.listdir('.') if (os.path.isfile(f) and "mfcc" in f)]
print("MFCC average =", cmavg(files))

files = [f for f in os.listdir('.') if (os.path.isfile(f) and "mfsc" in f)]
print("MFSC average =", cmavg(files))

files = [f for f in os.listdir('.') if (os.path.isfile(f) and "spec" in f)]
print("SPEC average =", cmavg(files))
