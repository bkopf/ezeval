#!/bin/python

# Run multiple evaluations using the same setup

import os
import re
import sys
from subprocess import call


scriptpath = os.path.dirname(os.path.realpath(__file__))
pat = re.compile("(.+)\.mdf")
mdffolder = "{}/../mdfs".format(scriptpath)

def run(fname, num):
    cmd = "cp {0}/{1}.mdf {0}/{1}{2}.mdf".format(mdffolder, fname, num)
    print(cmd.split())
    call(cmd.split())
    cmd = "echo \"$(date): Started evaluation of {}{} \" >> log.txt".format(fname, num)
    print(cmd)
    call(cmd.split())
    cmd = "{} evaluate.py {}{}".format(sys.executable, fname, num)
    print(cmd)
    call(cmd.split())

for evalnum in range(int(sys.argv[1])):
    run("cnn2-mfcc-sn", evalnum)
    run("cnn2-mfsc-sn", evalnum)
    run("cnn2-spec-sn", evalnum)


cmd = "cd {}/../results".format(scriptpath)
call(cmd.split())
cmd = "{} {}/../results/summit.py".format(sys.executable, scriptpath)
call(cmd.split())
