# Easy evaluation: Everything needed to automatically validate the architecture-feature map combination on a given dataset.
import sys
from keras.models import Model
from keras.layers import Input, Dense, Dropout, Conv1D, MaxPooling1D, Conv2D, MaxPooling2D, Flatten, Dense
import numpy as np

#1# Splitting data in training / validation / testing ########################################
def split_data(model_name):
    import os
    import re
    from random import randint
    from math import ceil

    val_ratio   = 0.25
    test_ratio  = 0.15
    # train ratio = 0.6

# ACTUALLY: Reading a format.txt file is NOT necessary! Remove this functionality
# Reads the filename format from a 'format.txt' file, then uses regex to apply this on all
# the remainding files in the ./audio/ folder (which should be wav-files only).
# These audio files are then divided randomly in a training set and a test set.

    with open("../mdfs/{}.mdf".format(model_name)) as f:
        lines = f.readlines()


# Get the database path and the naming convention regex
    for ln, line in enumerate(lines):
        lines[ln] = line.strip()

    database_path = ""
    regex = ""
    output_folder = "../mdfs" #for testing purposes only

    for ln, line in enumerate(lines):
        if(lines[ln] == "--- database ---"):
            database_path = lines[ln + 1]
        if(lines[ln] == "- regex -"):
            regex = lines[ln + 1]

    print(regex)
    print(database_path)

    p = re.compile(regex)
    filelist = sorted(os.listdir(database_path))

    class_labels = {}

    for filename in filelist:
        if(p.search(filename)):
            dc = p.search(filename).group(1)    # detected class
            # Here, we append the current file name to the list of the right class:
            class_labels.setdefault(dc, []).append(filename)    # because dictionary[key].append() does not work that way

### Pick 25/100 of the original files (> 25/85 remaining)
    val_files = {}
    for c in class_labels:
        num_samples = len(class_labels[c])
        for i in range(int(ceil(val_ratio*num_samples))):     # ceil because we'd rather have one too many than none
            random_sample = class_labels.setdefault(c, []).pop(randint(0, num_samples - i - 1))
            val_files.setdefault(c, []).append(random_sample)

### Pick 15/100 samples of each class randomly for test set ###
    test_ratio /= (1 - val_ratio)
    test_files = {}
    for c in class_labels:
        num_samples = len(class_labels[c])
        for i in range(int(ceil(test_ratio*num_samples))):     # ceil because we'd rather have one too many than none
            random_sample = class_labels.setdefault(c, []).pop(randint(0, num_samples - i - 1))
            test_files.setdefault(c, []).append(random_sample)

# The training data files are the remainding entries in class_labels:
# The class labels are the *keys* of the class_labels dictionary.
    i = 0
    train_data = ""
    labels = ""
    for c in class_labels:
        for filename in class_labels[c]:
            train_data += "{}:{}\n".format(i, filename)
        labels += "{}\n".format(c)
        i += 1
    print(labels)

# The validation data files are located in the val_data list:
    i = 0
    val_data = ""
    for c in val_files:
        for filename in val_files[c]:
            val_data += "{}:{}\n".format(i, filename)
        i += 1

# The testing data files are located in the test_files list:
    i = 0
    test_data = ""
    for c in test_files:
        for filename in test_files[c]:
            test_data += "{}:{}\n".format(i, filename)
        i += 1

# Write the file names to the mdf:
    with open("../mdfs/{}.mdf".format(model_name), 'a') as f:
        f.write("\n- labels -\n")
        f.write(labels)
        f.write("\n- training -\n")
        f.write(train_data)
        f.write("\n- validation -\n")
        f.write(val_data)
        f.write("\n- testing -\n")
        f.write(test_data)
        f.close()

    """     Appends the following information in the model description file:

    - labels -
    <class labels sorted alphabetically, one class per line>

    - training -
    <name of files to be used for training, one file name per line>

    - validation -

    - testing -
    <name of files to be used for testing, one file name per line>
    """

    return (train_data, val_data, test_data)
##############################################################################################

#2# Reading model description files ##########################################################
def read_mdf(model_name):
    # Using list to be able to use the same code for all
    architecture    = []
    ftype           = []
    database_path   = []
    regex           = []
    labels          = []
    train_files     = []
    val_files       = []
    test_files      = []

    section_dict = {"--- architecture ---": architecture,
            "--- features ---": ftype,
            "--- database ---": database_path,
            "- regex -": regex,
            "- labels -": labels,
            "- training -": train_files,
            "- validation -": val_files,
            "- testing -": test_files
            }

    with open("../mdfs/{}.mdf".format(model_name)) as f:
        lines = f.readlines()
        f.close()

    # If the data is not yet splitted, run split_data and read again:
    if "- labels -\n" not in lines:
        print("Splitting data")
        split_data(model_name)
        with open("../mdfs/{}.mdf".format(model_name)) as f:
            lines = f.readlines()
            f.close()

    line = ""   # current line in the mdf
    ln = 0      # line number
    while ln < len(lines):
        line = lines[ln].strip()
        # If a valid section is found, iterate through it until empty line
        if line in section_dict:
            section = line
            while(ln < len(lines) - 1):
                ln += 1
                line = lines[ln].strip()
                print(line)
                if line == '':
                    break
                section_dict.setdefault(section,[]).append(line)
                # ^ filter() to get rid of empty string entries. list() because filter() returns an iterator
        ln += 1

    architecture = [list(filter(None, line.split(' '))) for line in architecture]
    ftype = list(filter(None, ftype[0].split(' ')))   # only one line of feature type

    fmap = 0
    print(architecture)
    if 'conv2d' in architecture[0]:
        fmap = 2
    elif 'conv1d' in architecture[0]:
        fmap = 1
                                            # \/ "unlist" the types with only entry
    return (architecture, fmap, ftype, database_path[0], regex[0], labels, train_files, val_files, test_files)
##############################################################################################

#3# Making models out of the mdfs ############################################################

##### To implement new layer types, look at this section only ##
def add_relu(input_layer, *args):
    num_neurons = int(args[0])   # This is only for readability
    print("ReLU layer with {} neurons.".format(num_neurons))
    return Dense(num_neurons, activation="relu")(input_layer)

def add_dropout(input_layer, *args):
    rate = float(args[0])
    print("Dropout layer with rate {}".format(rate))
    return Dropout(rate)(input_layer)

def add_softmax(input_layer, *args):
    num_classes = int(args[0])
    print("Softmax layer with {} outputs".format(num_classes))
    return Dense(num_classes, activation="softmax")(input_layer)

def add_conv1d(input_layer, *args):
    num_filters = int(args[0])
    kern_size   = int(args[1])
    print("Conv1D layer with {} filters of size {} ".format(num_filters, kern_size))
    return Conv1D(filters=num_filters, kernel_size=kern_size)(input_layer)

def add_pool1d(input_layer, *args):
    print("MaxPooling1D layer with pooling size {}".format(args[0]))
    return MaxPooling1D(pool_size=3)(input_layer)

def add_conv2d(input_layer, *args):
    num_filters = int(args[0])
    kern_size   = int(args[1])
    print("Conv2D layer with {} filters of size {} ".format(num_filters, kern_size))
    return Conv2D(num_filters, kern_size, padding = 'same', data_format="channels_first")(input_layer)

def add_pool2d(input_layer, *args):
    print("MaxPooling1D layer with pooling size {}".format(args[0]))
    return MaxPooling2D(pool_size=3, data_format="channels_first")(input_layer)

def add_flat(input_layer, *args):
    print("Flatten layer")
    return Flatten()(input_layer)

######## !! The star (*) can be removed from all of these, as long as it is removed from the function call. Right now it's just unpacking and 'repacking'.

valid_layers = {
        "relu":     add_relu,
        "dropout":  add_dropout,
        "softmax":  add_softmax,
        "conv1d":   add_conv1d,
        "conv2d":   add_conv2d,
        "pool1d":   add_pool1d,
        "pool2d":   add_pool2d,
        "flat":     add_flat
        }

#################################################################

def make_model(architecture, train_x, num_classes):
    # architecture is a list of lists, where each list contains the name and parameters of a layer

    # ! If there is need for parameters from the code instead of the mdf, just include these for ALL of the function calls and ignore them in the functions that don't need them.
    # Not really efficient, but will probably simplify the mdf syntax a bit.
    model = None

    layers = [None]*(len(architecture) + 1) # make room for input layer
    input_shape = train_x.shape[1:]
    layers[0] = Input(shape=input_shape)
    print("Input layer with shape {}".format(input_shape))
    l = 0
    while l < len(architecture):
        line = architecture[l]
        layer_type = line[0]
        if(layer_type in valid_layers):
            if layer_type == "softmax":     # to avoid giving the num_classes "manually"
                layers[l + 1] = valid_layers[layer_type](layers[l], num_classes)
            else:
                layers[l + 1] = valid_layers[layer_type](layers[l], *line[1:])
        else:
            print("Invalid line detected ({})! Exiting...".format(layer_type))
            sys.exit()
        l += 1
    return Model(layers[0], layers[-1])
##############################################################################################

#4# Confusion matrix code ####################################################################
def make_cm_pdf(cm, mdf, labels):
    from subprocess import call     # <- to automatically typeset latex table code
# Make LaTeX code for confusion matrix and typeset to pdf.
# Might be useful for writing report in the future. Use format_cm() for pure analysis, though.
    num_classes = len(cm)

    latex_code = """% Automatically generated latex table
\\documentclass{{standalone}}
\\usepackage{{arev}}
\\usepackage[T1]{{fontenc}}
\\begin{{document}}
    \\begin{{tabular}}{{|c|{}}}
    \\hline""".format('c|'*num_classes)

        # Make horisontal class labels:
    for c in range(num_classes):
        latex_code += "\t\t\t& \\textbf{{{}}} ".format(labels[c].strip())
    latex_code += "\\\\ \\hline\n"

    for c in range(num_classes):
        latex_code += "\t\t\\textbf{{{}}} ".format(labels[c].strip())    # Correct class label
        for acc in cm[c]:
            latex_code += "& {0:.1f} ".format(acc)
        latex_code += "\\\\ \\hline\n"

    latex_code += """\t\\end{tabular}
\\end{document}"""

    with open('./results/latex/{}.tex'.format(mdf),'w') as f:
        f.write(latex_code)

    # Typeset the confusion matrix pdf:
    call("pdflatex -output-directory=results/latex ./results/latex/{}".format(mdf).split(' '))
    call("mv ./results/latex/{0}.pdf ./results/{0}.pdf".format(mdf).split(' '))
    call("mupdf ./results/{}.pdf".format(mdf).split(' '))
    print("Confusion matrix saved as \'{}.pdf\' in results folder.".format(mdf))

def make_cm_txt(ccm, labels):
# !! Please do not try to interpret this ugly code
    cm = ccm[0]
    precision = ccm[1]
    recall  = ccm[2]
    num_labels = len(labels)
    fmt_text = ""

    # Decide size of each table cell (minimum 6):
    cell_size = 8
    for lab in labels:
        if (len(lab)+2) > cell_size:
            cell_size = len(lab) + 2

    # Print initial borders and labels:
    fmt_text += ' '*(cell_size + 2) + '-'*((cell_size+1)*(num_labels)) + '-----------\n'
    fmt_text += ' '*(cell_size + 1) + '|'
    for lab in labels:
        fmt_text += "{:^{cs}}|".format(lab, cs=cell_size)

    fmt_text += " precision |"
    fmt_text += '\n ' + ('-'*cell_size + '|')*(num_labels+1) + '-----------|\n'
    for r, row in enumerate(cm):
        fmt_text += "|{:^{cs}}|".format(labels[r], cs=cell_size)
        for col in row:
            fmt_text += "{:>{cs}} |".format(int(col), cs=cell_size-1)
        fmt_text += "{:>{cs}.2f}% |".format(100*precision[r], cs=9)
        fmt_text += '\n|' + ('-'*cell_size + '|')*(num_labels+1) + '-----------|\n'

    fmt_text += "|{:^{cs}}|".format('recall', cs=cell_size)
    for r in recall:
            fmt_text += "{:>{cs}.2f}% |".format(100*r, cs=cell_size-2)


    fmt_text += '\n ' + ('-'*cell_size + '-')*(num_labels) + '-'*cell_size + '|'

    return fmt_text

def gen_cm(actual, predicted, num_classes, percents=False):
# Makes a confusion matrix out of the given predictions and correct classes.
    cm = np.zeros((num_classes, num_classes))
    cc_samples = [0]*num_classes    # Counts the number of testing samples of each class

    class_precision = [0]*num_classes
    class_recall    = [0]*num_classes

    for i in range(len(actual)):
        pred    = np.argmax(predicted[i])
        true    = np.argmax(actual[i])
        cm[pred][true] += 1             # row marks prediction, col marks true class
        cc_samples[true] += 1

    for p, pred in enumerate(cm):
        if sum(pred) == 0:
            class_precision[p] = 0
        else:
            class_precision[p] = cm[p][p] / sum(pred)       # cm[p][p] = True Positive?

    for t, true in enumerate(np.transpose(cm)):
        # We know that we have more than 0 *true* classes. no need to check
        class_recall[t] = cm[t][t] / sum(true)

    # Probably unnecessary when you gott class precision and recall in place:
    if (percents):
        for true in range(num_classes):
            for pred in range(num_classes):
                cm[true][pred] *= 100/cc_samples[corr]

    return (cm, class_precision, class_recall)

# General accuracy measure:
   #     print("Class {}, predict {}".format(corr, pred))
   #     if (corr == pred):
   #         correct += 1

   # acc = correct / len(test_y)
   # print("Accuracy: {}%".format(100 * acc))


   # scores = model.evaluate(test_x, test_y, batch_size=32)
   # print("\n%s: %.2f%%" % (model.metrics_names[1], scores[1]*100))
   # #print(model.predict_classes(test_x, batch_size=32))

##############################################################################################

#5# Save results #############################################################################
def save_results(model_name, labels, cm, precision, recall, train_acc, val_acc, test_acc):
    # Print and save the results with the architecture description:
    results = ""
    with open('../mdfs/{}.mdf'.format(model_name), 'r') as f:
        lines = f.readlines()
        line = ""   # current line in the mdf
        ln = 0      # line number
        while ln < len(lines) and "- regex -" not in line:
            results += line
            line = lines[ln]
            ln += 1
    results += "--- results ---"
    results += "\nTraining accuracy: {:.2f}%".format(train_acc*100)
    results += "\nValidation accuracy: {:.2f}%".format(val_acc*100)
    results += "\nTesting accuracy: {:.2f}%".format(test_acc*100)
    print("\n" + results)

    # Generate the confusion matrix:
    results += "\nConfusion matrix for testing:\n"
    results += make_cm_txt((cm, precision, recall), labels)
    with open('../results/{}.cm'.format(model_name), 'w') as f:
        f.write(results)
        f.close()
    print("Confusion matrix saved to {}.cm".format(model_name))

    # Print results to .csv too, for later analysis:
    csv_string = '"class", "precision", "recall"\n'
    for l, lab in enumerate(labels):
        csv_string += '"{}", {}, {}\n'.format(lab, precision[l], recall[l])
    csv_string += '\n"train acc", {}\n'.format(train_acc)
    csv_string += '"val acc", {}\n'.format(val_acc)
    csv_string += '"test acc", {}\n'.format(test_acc)
    with open('../results/{}.csv'.format(model_name), 'w') as f:
        f.write(csv_string)
##############################################################################################
