#
#   Everything to do with extracting features in the project
#
import numpy as np
#import matplotlib.pyplot as plt
import scipy.io.wavfile as wav
from scipy.misc import imresize
from scipy.signal import spectrogram
import psf

def print_spec(spectrogram):
    print("shape of spectrogram: {}".format(spectrogram.shape)) # to make sure the dimensions are right
    plt.pcolormesh(spectrogram)
    plt.xlabel('Frequency band')
    plt.ylabel('Frame')
    plt.title('Spectrogram')
    plt.show()

def get_feature_maps(database_path, file_list, feature_description, fmap):
    # For each file in file_list: Get classes and extract features according to feature_type
    ftype    = feature_description[0][:4]
    if len(feature_description[0]) > 4: # mfcc<whatever> = delta feats
        use_delta = True
        fmap = "{}d".format(fmap)
    else:
        use_delta = False
    time_res = int(feature_description[1])     # number of context frames
    freq_res = int(feature_description[2])
    frep = "res"    # feature representation
    if len(feature_description) > 3:
        frep = feature_description[3]

    nfft = 2*freq_res-1 if freq_res % 2 == 0 else 2*freq_res-2  # for use with spec
    # fmap is automatically given by the architecture:
    featmap_shape = {
            "0d": np.empty((0, 3*time_res*freq_res)),  # corresponds to DNNs
            "1d": np.empty((0, 3*time_res, freq_res)), # corresponds to 1DCNNs
            "2d": np.empty((0, 3, time_res, freq_res)), # corresponds to 2DCNNs
            0: np.empty((0, 1*time_res*freq_res)),  # These are for non-delta
            1: np.empty((0, 1*time_res, freq_res)),
            2: np.empty((0, 1, time_res, freq_res))
            }
    feature_func = {
            "mfcc": lambda s,r,freqbands: psf.mfcc(s, r, preemph=0,
                numcep=freqbands, nfilt=2*freqbands, winfunc=np.hamming,  nfft=1024),
            "mfsc": lambda s,r,freqbands: psf.mfsc(s, r, preemph=0,
                nfilt=freqbands, winfunc=np.hamming, nfft=1024),
            "spec": lambda s,r,*args: psf.spec(s, r, preemph=0, winfunc=np.hamming, nfft=nfft),
            }
    if ftype not in feature_func or fmap not in featmap_shape:
        print("The given feature map representation is invalid")
        sys.exit()

    train_x = featmap_shape[fmap]
    train_y = []
    print(ftype)
    for audiofile in file_list:
        c, filename = audiofile.strip().split(':')
        print("Extracting features from file {}".format(filename))
        (rate, sig) = wav.read("{}/{}".format(database_path, filename))

        # Make spectrogram and pad with zeros to get a multiple of <frame size>:
        spectrogram = feature_func[ftype](sig, rate, freq_res)

##### This section was written in a hurry, and needs a clean up #####

# If "res" (resized spectrogram) is chosen:
        if frep == "res":
            if use_delta:
                features = np.empty((3, time_res, freq_res))
                # Consider adding an option for nfilt (number of filters) \/
                features[0] = imresize(spectrogram, (time_res, freq_res))
                features[1] = imresize(psf.delta(spectrogram, 1), (time_res, freq_res))
                features[2] = imresize(psf.delta(spectrogram, 2), (time_res, freq_res))

                # Reshape the features into the correct feature map
                if fmap == '0':
                    features = np.array([freq for channel in features for frame in channel for freq in frame])
                elif fmap == '1':
                    # 3*<time_res> number of vectors with size <freq_res>
                    remapped_feat = np.empty((3*time_res, freq_res))
                    for i in range(time_res):
                        remapped_feat[3*i]   = features[0][i]
                        remapped_feat[3*i+1] = features[1][i]
                        remapped_feat[3*i+2] = features[2][i]
                                        #   ^ [channel][frame]
                    features = remapped_feat
                else:
                    train_x = np.append(train_x, [features], axis=0)
                train_y.append(int(c))
            else:
                features = np.empty((1, time_res, freq_res))
                # Consider adding an option for nfilt (number of filters) \/
                features[0] = imresize(spectrogram, (time_res, freq_res))

                # Reshape the features into the correct feature map
                if fmap == '0':
                    features = np.array([freq for frame in features for freq in frame])
                elif fmap == '1':
                    # 3*<time_res> number of vectors with size <freq_res>
                    remapped_feat = np.empty((time_res, freq_res))
                    for i in range(time_res):
                        remapped_feat[3*i]   = features[0][i]
                                        #   ^ [channel][frame]
                    features = remapped_feat
                else:
                    train_x = np.append(train_x, [features], axis=0)
                train_y.append(int(c))

# If "pad" (padded spectrogram) is chosen:
        elif frep == "pad":
            print("pad good")

# If "seg" (spectrogram segments) is chosen:
        else:
            numframes = int(np.ceil(len(spectrogram) / time_res))
            spectrogram = np.pad(spectrogram, ((0, time_res*numframes - len(spectrogram)),(0,0)), 'constant', constant_values=0)

            # Reshape features (include delta-features)
            if use_delta:
                delta       = psf.delta(spectrogram, 1)
                deltadelta  = psf.delta(spectrogram, 2)

                features = np.empty((3, time_res, freq_res))
                for specframe in range(numframes):
                    features[0] = spectrogram[time_res*specframe : time_res*(specframe+1)]
                    features[1] = delta[time_res*specframe : time_res*(specframe+1)]
                    features[2] = deltadelta[time_res*specframe : time_res*(specframe+1)]

                    if fmap == 0:
                        remapped_feat = np.array([freq for channel in features for frame in channel for freq in frame])
                        train_x = np.append(train_x, [remapped_feat], axis=0)
                    elif fmap == 1:
                        # 3*<time_res> number of vectors with size <freq_res>
                        remapped_feat = np.empty((3*time_res, freq_res))
                        for i in range(time_res):
                            remapped_feat[3*i]   = features[0][i]
                            remapped_feat[3*i+1] = features[1][i]
                            remapped_feat[3*i+2] = features[2][i]
                        train_x = np.append(train_x, [remapped_feat], axis=0)
                    else:
                        train_x = np.append(train_x, [features], axis=0)
                    train_y.append(int(c))
            # Reshape features without delta-features
            else:
                features = np.empty((1, time_res, freq_res))
                for specframe in range(numframes):
                    features[0] = spectrogram[time_res*specframe : time_res*(specframe+1)]
                    if fmap == 0:
                        # Remove channel from this, since there is only one channel here:
                        remapped_feat = np.array([freq for channel in features for frame in channel for freq in frame])
                        train_x = np.append(train_x, [remapped_feat], axis=0)
                    elif fmap == 1:
                        remapped_feat = np.empty((time_res, freq_res))
                        for i in range(time_res):
                            remapped_feat[i]   = features[0][i]
                        train_x = np.append(train_x, [remapped_feat], axis=0)
                    else:
                        train_x = np.append(train_x, [features], axis=0)
                    train_y.append(int(c))

    print("{} delta features".format("Using" if use_delta else "Not using"))
    print("Using feature representation '{}'".format(frep))
    print("shape of feature list: {}".format(train_x.shape))
    #wait = input("Press enter to continue")
    return (train_x, train_y)
