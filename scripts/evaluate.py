#!/bin/python

import sys
import os
import numpy as np
import ezeval
import exfeat

from keras.utils import to_categorical

def main():
    if len(sys.argv) < 2:
        print("Use: evaluate.py <model_name>")
        sys.exit()

    model_name = sys.argv[1]

    architecture, feature_map, feature_description, database_path, regex, labels, train_files, val_files, test_files = ezeval.read_mdf(model_name)
    print("featmap = {}".format(feature_map))

    ### Make feature vectors and their corresponding classes:
    train_x, train_y = exfeat.get_feature_maps(database_path, train_files, feature_description, feature_map)
    val_x, val_y = exfeat.get_feature_maps(database_path, val_files, feature_description, feature_map)
    test_x, test_y = exfeat.get_feature_maps(database_path, test_files, feature_description, feature_map)
    num_classes = max(train_y)+1
    print("train size = {}".format(len(train_x)))
    print("val size = {}".format(len(val_x)))
    print("test size = {}".format(len(test_x)))
    #wait = input('press enter')
    train_y = to_categorical(train_y, num_classes = num_classes)
    val_y = to_categorical(val_y, num_classes = num_classes)
    test_y = to_categorical(test_y, num_classes = num_classes)

    ### Automatically generate model from the model description
    model = ezeval.make_model(architecture, train_x, num_classes)

    ### Compile and train the model
    model.compile(loss='categorical_crossentropy',
                  optimizer='adam',
                  metrics=['accuracy'])
    num_epochs = 100    # consider making this an argument. also consider early stop!
    print(test_x.shape)
    print(train_x.shape)
    print(val_x.shape)
    model.summary()
    model.fit(train_x,
              train_y,
              epochs = num_epochs,
              batch_size=32,
              validation_data=(val_x, val_y))
    model.save("../modelfiles/{}.h5".format(model_name))

    # Evaluate the model, generate the confusion matrix and save the results
    train_scores = model.evaluate(train_x, train_y)
    val_scores = model.evaluate(val_x, val_y)
    test_scores = model.evaluate(test_x, test_y)
    predicts = model.predict(test_x)
    (cm, precision, recall) = ezeval.gen_cm(test_y, predicts, num_classes)
    ezeval.save_results(model_name, labels, cm, precision, recall, train_scores[1], val_scores[1], test_scores[1])

if __name__ == "__main__":
    main()
