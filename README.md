ezeval framework
================

## Use   

### Example evaluation using spoken\_numbers
- Edit the example mdfs in ezeval/mdfs/ so that the *database* entry contains the full path to the *spoken\_numbers* dataset.
- Change directory to ezeval/scripts/
- Perform a single evaluation:
	```python evaluate.py cnn2-mfcc-sn.mdf
    ```
- or perform multiple (for instance 5) evaluations:
	```python multi.py 5
    ```

### To perform a single evaluation:
- Make <model_description>.mdf in ezeval/mdfs/
- Perform the evaluation: 
    ```python evaluate.py <model_description>
    ```

### To perform multiple evaluations with the same setup and calculate average:
- Edit multi.py (in ezeval/scripts/) so that it runs the wanted mdfs.
- Run the multi script:
	```multi.py <number of evaluations per input file> 
    ```
- Change directory to ezeval and run the summit script:
	python summit.py
